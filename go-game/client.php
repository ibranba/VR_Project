<!DOCTYPE HTML>
<html>
    <head>
        <title>Go game</title>
        <script type="text/javascript" src="wgo/wgo.js"></script> <!-- linking WGo javascript -->
    </head>
    <body>
        <div id="board">
            <!-- board will go here -->
        </div>
        <script>
            var game = new WGo.Game(19, "KO");
            var board = new WGo.Board(document.getElementById("board"), {
                width: 600
            });

            board.addEventListener("click", function(x, y) {
                 board.addObject({
                    x: x,
                    y: y,
                    c: WGo.W
                });
            });

            var conn = new WebSocket('ws://localhost:9999');
            
            conn.onopen = function(e) {
                console.log("Connection established!");
            };

            conn.onmessage = function(e) {
                var coordinate = e.data.split(';');
                console.log(coordinate);
                var i = parseInt(coordinate[0], 10);
                var j = parseInt(coordinate[1], 10);

                if (game.isValid(i, j, WGo.B))
                {
                    game.play(i, j, WGo.B);
                    board.addObject({
                        x: i,
                        y: j,
                        c: WGo.B
                    });
                    playAI(i , j);
                } 
                else 
                {
                    conn.send("NOK");
                    console.log("tu ne peux pas jouer là bro");
                } 
            };

            var playAI = function(l , c) {
                console.log('je suis l AI lol');
                var i = 0, j = 0;

                do {
                    i = random(0, 19); 
                    j = random(0, 19);
                } while(!game.isValid(i, j, WGo.W));

                console.log("je joue en ", i, j);
                game.play(i, j, WGo.W);
                board.addObject({
                    x: i,
                    y: j,
                    c: WGo.W
                });
                conn.send(i + ";" + j + ";" + l + ";" + c);
                
            }

            var random = function(min, max) {
                return Math.floor(Math.random() * (max - min + 1)) + min;
            }

        </script>
    </body>
</html>