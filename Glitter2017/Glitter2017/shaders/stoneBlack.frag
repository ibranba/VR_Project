#version 330 core // VERSION OF OPENGL 3.3
out vec4 FragColor;
in vec2 TexCoord;
uniform sampler2D texture1;
void main() {                                               // Main function    
	FragColor = texture(texture1, TexCoord);
}