# Run
1. Go to `go-game` folder.
2. Execute `start_server.bat` file (it launches the websocket server)
Note : You must have PHP in your `PATH` variable.

3. Double-click on `client.php` (it is a client)
4. Run the program.

Note : You can also directly run the program but you'll not be able to play against someone else. You will just be able to place black go ishis on the board.

# How to play
1. Press `tab` to change the viewpoint. You can only play with the camera standing above the board.
2. You can aim the desired board's intersection by moving the mouse on it. A green ray will show where the go ishi would be placed if you press `space`.
3. Press `space` to play your move there.