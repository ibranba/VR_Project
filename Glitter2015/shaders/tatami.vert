#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 itexCoord;

out vec2 TexCoord;

uniform mat4 view;
uniform mat4 projection;
uniform mat4 model;

void main() {  
	  gl_Position =  projection * view *  model * vec4(position/1000, 1.0);
	  TexCoord = (vec2(itexCoord.x * 50, itexCoord.y * 50));
}

