#version 330 core // VERSION OF OPENGL 3.3
out vec4 FragColor;

struct Material {
    sampler2D diffuse;
    vec3 specular;    
    float shininess;
};
 
in vec2 TexCoord;
uniform sampler2D texture1;

void main() {                        
	FragColor = texture(texture1, TexCoord);
}

