#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include "GoGame.h"
#include "RayPicker.hpp"

class Events
{
private:
	Camera *camera;
	RayPicker *rayCast;

	GLFWwindow *window;

	bool keys[1024];
	float deltaTime;
	bool firstMouse;

	GLfloat lastX;
	GLfloat lastY;

	GLuint screenWidth;
	GLuint screenHeight;

	static void staticKeyHandler(GLFWwindow* window, int key, int, int action, int);
	static void staticMouseButtonHandler(GLFWwindow*, int button, int action, int);
	static void staticCursorPositionHandler(GLFWwindow* window, double xpos, double ypos);
	static void staticScrollHandler(GLFWwindow* window, double xoffset, double yoffset);

	void defaultKeyHandler(int key, int scancode, int action, int mods);
	void defaultMouseButtonHandler(int button, int action, int mods);
	void defaultCursorPositionHandler(double xpos, double ypos);
	void defaultScrollHandler(double xoffset, double yoffset);


	// singleton
	static Events *instance;
	Events(Camera *c, GLFWwindow *w, RayPicker *r, GLuint screenWidth, GLuint screenHeight);
	glm::vec3 computeIntersectionWithGoBan(glm::vec3 p0, glm::vec3 p1);
public:
	~Events();
	int indexCurrentCamera;
	// Singleton is accessed via getInstance()
	static Events &getInstance(); 
	
	// We should only use this once the 
	// camera and window are initialized in order to 
	// create a working Events object
	static Events &initializeInstance(Camera *c, GLFWwindow *w, RayPicker *rayCast, GLuint screenWidth, GLuint screenHeight);

	void setKeyListener(GLFWkeyfun handler = NULL);
	void setCursorPositionListener(GLFWcursorposfun handler = NULL);
	void setMouseButtonListener(GLFWmousebuttonfun handler = NULL);
	void setScrollEventListener(GLFWscrollfun handler = NULL);

	void pollEvents();
	void applyMovements();
	void showFPS();
};
