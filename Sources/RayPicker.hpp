#include "Camera.hpp"
#include <iostream>

class RayPicker {
	private:
		Camera *camera;
		glm::mat4 projection;
		glm::mat4 view;
		float screenWidth;
		float screenHeight;
	
	public:
		glm::vec3 currentRay{ 0.0f, 0.0f, 0.0f };
		RayPicker(Camera *c, glm::mat4 &p, float screenWidth, float screenHeight);
		glm::vec3 updateRay(double xpos, double ypos);
};
