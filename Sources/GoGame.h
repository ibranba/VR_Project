#include <iostream>
#include <winsock2.h>
#include <assert.h>
#include <stdio.h>
#include <string>
#include <vector>
#include "easywsclient.hpp"

class GoGame
{
	private:
		const int port = 9999;
		const std::string wsAddress{ "ws://localhost" };
		// singleton
		static GoGame *instance;
		GoGame();
		bool terminated;

	public:
		~GoGame();
		easywsclient::WebSocket::pointer ws;
		// Cette fonction est appel�e lors de la r�ception d'un message
		void handleMessage(const std::string & message);
		void mainloop();
		void send(const std::string& message);
		void terminate();

		char goBan[19][19];

		bool playAt(int i, int j, char goIshi);
		
		// Singleton is accessed via getInstance()
		static GoGame &getInstance();
		static void handle_message(const std::string & message);

		std::vector<std::string> split(const std::string& str, const std::string& delim)
		{
			std::vector<std::string> tokens;
			size_t prev = 0, pos = 0;
			do
			{
				pos = str.find(delim, prev);
				if (pos == std::string::npos) pos = str.length();
				std::string token = str.substr(prev, pos - prev);
				if (!token.empty()) tokens.push_back(token);
				prev = pos + delim.length();
			} while (pos < str.length() && prev < str.length());
			return tokens;
		}
};