#include "AudioMaster.h"
#include <Windows.h> 
#include <mmsystem.h>
#include <vector>
#include <exception>


#define TEST_ERROR(_msg)		\
	error = alGetError();		\
	if (error != AL_NO_ERROR) {	\
		fprintf(stderr, _msg " ");	\
		std::cout << error << std::endl;	\
	}

#define DR_WAV_IMPLEMENTATION
#include "dr_wav.h"

AudioMaster * AudioMaster::instance = NULL;

AudioMaster::AudioMaster()
{
	init();
}


AudioMaster::~AudioMaster()
{
}

AudioMaster & AudioMaster::getInstance()
{
	if (AudioMaster::instance == NULL)
		AudioMaster::instance = new AudioMaster();

	return *instance;
}

void AudioMaster::init()
{
	alcOpenDevice((ALchar*)"DirectSound3D");
	device = alcOpenDevice(NULL);
	if (!device)
	{
		fprintf(stderr, "Oops\n");
		exit(-1);
	}
	context = alcCreateContext(device, NULL);
	alcMakeContextCurrent(context);
	if (!context)
	{
		fprintf(stderr, "Oops2\n");
		exit(-1);
	}
	loadBgMusic();
}

void AudioMaster::cleanUp() {
	alDeleteSources(1, &sourceGoBan);
	alDeleteBuffers(1, &bufferGoBan);
	device = alcGetContextsDevice(context);
	alcMakeContextCurrent(NULL);
	alcDestroyContext(context);
	alcCloseDevice(device);
}
void AudioMaster::setUpListener(std::vector<ALfloat> & listenerOri, std::vector<ALfloat> & listenerPos) {
	//ALfloat listenerOri[] = { 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f };
	alListener3f(AL_POSITION, listenerPos[0], listenerPos[1], listenerPos[2]);
	// check for errors
	alListener3f(AL_VELOCITY, 0, 0, 0);
	// check for errors
	alListenerfv(AL_ORIENTATION, &listenerOri[0]);
}
void AudioMaster::configSources(std::vector<ALfloat> & pos) {

	alGenSources((ALuint)1, &sourceGoBan);
	TEST_ERROR("alGenSources((ALuint)1, &sourceGoBan);");

	alSourcef(sourceGoBan, AL_PITCH, 1);
	TEST_ERROR("alSourcef(sourceGoBan, AL_PITCH, 1);");
	alSourcef(sourceGoBan, AL_GAIN, 1);
	TEST_ERROR("alSourcef(sourceGoBan, AL_GAIN, 1);");

	alSource3f(sourceGoBan, AL_POSITION, 0, 0, 0);
	TEST_ERROR("alSource3f(sourceGoBan, AL_POSITION, 0, 0, 0);");

	alSource3f(sourceGoBan, AL_VELOCITY, 0, 0, 0);
	TEST_ERROR("alSource3f(sourceGoBan, AL_VELOCITY, 0, 0, 0);");

	alSourcei(sourceGoBan, AL_LOOPING, AL_FALSE);
	TEST_ERROR("alSourcei(sourceGoBan, AL_LOOPING, AL_FALSE);");


	alGenBuffers((ALuint)1, &bufferGoBan);

	drwav* pWav = drwav_open_file("wav/stone_placed.wav");
	if (pWav == NULL) {
		std::cerr << "Background sound file not found \n" ;
		return;
	}
	int16_t* pSampleData = (int16_t*)malloc((size_t)pWav->totalSampleCount * sizeof(int16_t));
	drwav_read_s16(pWav, pWav->totalSampleCount, pSampleData);


	alBufferData(bufferGoBan, to_al_format(pWav->channels, pWav->bitsPerSample),
		pSampleData,
		pWav->bytesPerSample * pWav->totalSampleCount,
		pWav->sampleRate);

	alSourcei(sourceGoBan, AL_BUFFER, bufferGoBan);
	TEST_ERROR("buffer binding");
}

void AudioMaster::playStoneSound() {
	std::cout << "Play Sound" << std::endl;
	alSourcePlay(sourceGoBan);
	TEST_ERROR("source playing");
}

void AudioMaster::list_audio_devices(const ALCchar *devices)
{
	const ALCchar *device = devices, *next = devices + 1;
	size_t len = 0;

	fprintf(stdout, "Devices list:\n");
	fprintf(stdout, "----------\n");
	while (device && *device != '\0' && next && *next != '\0') {
		fprintf(stdout, "%s\n", device);
		len = strlen(device);
		device += (len + 1);
		next += (len + 2);
	}
	fprintf(stdout, "----------\n");
}

void AudioMaster::loadWavFile() {

}

bool AudioMaster::isPlaying() {
	alGetSourcei(sourceGoBan, AL_SOURCE_STATE, &sourceGoBanState);
	// check for errors
	if (sourceGoBanState == AL_PLAYING)
		return true;
	else
		return false;
}
void AudioMaster::loadBgMusic() {
	alGenSources((ALuint)1, &sourceBg);
	TEST_ERROR("alGenSources((ALuint)1, &sourceBg);");

	alSourcef(sourceBg, AL_PITCH, 1);
	TEST_ERROR("alSourcef(sourceBg, AL_PITCH, 1);");
	alSourcef(sourceBg, AL_GAIN, 0.1);
	TEST_ERROR("alSourcef(sourceBg, AL_GAIN, 1);");

	alSource3f(sourceBg, AL_POSITION, 0, 0, 0);
	TEST_ERROR("alSource3f(sourceBg, AL_POSITION, 0, 0, 0);");

	alSource3f(sourceBg, AL_VELOCITY, 0, 0, 0);
	TEST_ERROR("alSource3f(sourceBg, AL_VELOCITY, 0, 0, 0);");

	alSourcei(sourceBg, AL_LOOPING, AL_TRUE);
	TEST_ERROR("alSourcei(sourceBg, AL_LOOPING, AL_TRUE);");

	alSourcei(sourceBg, AL_SOURCE_RELATIVE, AL_TRUE);

	alSourcef(sourceBg, AL_ROLLOFF_FACTOR, 0.0);


	alGenBuffers((ALuint)1, &bufferBg);

	drwav* pWav = drwav_open_file("wav/Kami_no_Itte.wav");
	if (pWav == NULL) {
		exit(-1);
	}
	int16_t* pSampleData = (int16_t*)malloc((size_t)pWav->totalSampleCount * sizeof(int16_t));
	drwav_read_s16(pWav, pWav->totalSampleCount, pSampleData);


	alBufferData(bufferBg, to_al_format(pWav->channels, pWav->bitsPerSample),
		pSampleData,
		pWav->bytesPerSample * pWav->totalSampleCount,
		pWav->sampleRate);

	alSourcei(sourceBg, AL_BUFFER, bufferBg);
	TEST_ERROR("buffer binding");
}
void AudioMaster::playBackgroundMusic() {
	alSourcePlay(sourceBg);
	TEST_ERROR("source playing");
}
void AudioMaster::pauseBackgroundMusic() {
	alSourcePause(sourceBg);
	TEST_ERROR("source playing");
}
