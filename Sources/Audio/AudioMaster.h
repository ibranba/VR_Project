#pragma once
#include "OpenAL/al.h"
#include "OpenAL/alc.h"
#include <iostream>
#include <vector>
class AudioMaster
{
	static AudioMaster *instance;
	ALCdevice *device;
	ALCenum error;
	ALCcontext *context;
	ALuint sourceBg;
	ALuint sourceGoBan;
	ALint sourceGoBanState;
	ALuint bufferGoBan;
	ALuint bufferBg;
	ALuint blackBowlGoBan;
	ALuint whiteBowlGoBan;
	static void list_audio_devices(const ALCchar *devices);
	void loadWavFile();
	void loadBgMusic();

	
public:
	AudioMaster();
	~AudioMaster();
	static AudioMaster &getInstance();
	void init();
	void cleanUp();
	void setUpListener(std::vector<ALfloat> & listenerOri, std::vector<ALfloat> & listenerPos);
	void configSources(std::vector<ALfloat> & pos);
	void playStoneSound();
	bool isPlaying();
	void playBackgroundMusic();
	void pauseBackgroundMusic();
};

static inline ALenum to_al_format(short channels, short samples)
{
	bool stereo = (channels > 1);

	switch (samples) {
	case 16:
		if (stereo) {
			std::cout << "AL_FORMAT_STEREO16" << std::endl;
			return AL_FORMAT_STEREO16;
		}
		else {

			std::cout << "AL_FORMAT_MONO16" << std::endl;
			return AL_FORMAT_MONO16;
		}
	case 8:
		if (stereo)
			return AL_FORMAT_STEREO8;
		else
			return AL_FORMAT_MONO8;
	default:
		std::cout << "NOT 16 bit" << std::endl;
		return -1;
	}
}