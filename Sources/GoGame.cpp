#include "GoGame.h"
#include "Audio/AudioMaster.h"

GoGame * GoGame::instance = NULL;

using namespace easywsclient;

GoGame::GoGame()
{
	for (int i = 0; i < 19; i++)
		for (int j = 0; j < 19; j++)
			goBan[i][j] = '.';

	#ifdef _WIN32
	INT rc = -1;
	WSADATA wsaData;
	rc = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (rc) {
		printf("WSAStartup Failed.\n");
	}
	#endif
	this->ws = WebSocket::from_url(this->wsAddress + ":" + std::to_string(this->port));
	this->terminated = this->ws == NULL;
}

GoGame::~GoGame()
{
	this->ws->close();
	delete this->ws;

	#ifdef _WIN32
		WSACleanup();
	#endif
}

void GoGame::handleMessage(const std::string & message)
{
	if (message.compare("NOK") != 0) {
		std::vector<std::string> position = GoGame::split(message, ";");
		int i = std::stoi(position[0]);
		int j = std::stoi(position[1]);
		int l = std::stoi(position[2]);
		int c = std::stoi(position[3]);
		GoGame::getInstance().playAt(l, c, 'X');
		AudioMaster::getInstance().playStoneSound();
		Sleep(1000);
		GoGame::getInstance().playAt(i, j, 'O');
		AudioMaster::getInstance().playStoneSound();
	}
}

void GoGame::mainloop()
{
	while(!this->terminated && (this->ws->getReadyState() != WebSocket::CLOSED))
	{
		this->ws->poll();
		this->ws->dispatch(handle_message);
	}
}

void GoGame::send(const std::string & message)
{
	if(!this->terminated)
		ws->send(message);
}

void GoGame::terminate()
{
	this->terminated = true;
}

bool GoGame::playAt(int i, int j, char goIshi)
{
	if (0 <= i < 19 && 0 <= j < 19 && goBan[i][j] == '.') {
		goBan[i][j] = goIshi;
		return true;
	}
	return false;
}

GoGame & GoGame::getInstance()
{
	if (GoGame::instance == NULL)
		GoGame::instance = new GoGame();

	return *instance;
}

void GoGame::handle_message(const std::string & message)
{
	GoGame::getInstance().handleMessage(message);
}
