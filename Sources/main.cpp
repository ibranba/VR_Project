// Local Headers
#include "glitter.hpp"

// System Headers
#include <GLFW/glfw3.h>
#include <glm/gtc/matrix_transform.hpp>

// Standard Headers
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <thread>
#include <Windows.h>
#include <mmsystem.h>


#include "Audio/AudioMaster.h"
#include "SkyBox.hpp"
#include "Shader.hpp"
#include "Model.hpp"
#include "Events.hpp"

AudioMaster &audioMaster = AudioMaster::getInstance();

// Pour la cam�ra -- libre
Camera camera(glm::vec3(0.0f, 0.0f, 0.0f),
			  glm::vec3(0.0f, 1.0f, 0.0f), 
			  90.0f, 
			  0.0f);

// Camera pointa vers le goban
Camera cameraHaut(glm::vec3(1.68178f, 0.28f, 1.509f),
					glm::vec3(0.0f, 1.0f, 0.0f),
					-90.0f,
					-90.0f);

// Camera pointa vers le goban -- angle swagg
Camera cameraSwagg(glm::vec3(1.43f, -0.25f, 1.19f),
					glm::vec3(0.0f, 1.0f, 0.0f),
					57.0f,
					-10.0f);

GLfloat lastX = 400, lastY = 300;

// lighting
glm::vec3 lightPos(3.4f, -0.40f, 4.9f);

// default shaders properties
glm::vec3 lightAmbienteness{ 0.7f, 0.7f, 0.7f };
glm::vec3 lightDiffuseness{ 1.0f, 1.0f, 1.0f };
glm::vec3 lightSpeculareness{ 0.2f, 0.2f, 0.2f };

glm::vec3 materialSpeculareness{ 0.5f, 0.5f, 0.5f };
GLfloat materialShininess = 16.0f;

const unsigned int SCR_WIDTH = 1280;
const unsigned int SCR_HEIGHT = 720;

glm::mat4 getCurrentMatrixView();
glm::mat4 whiteStoneGoBanToRealCoordinates(glm::mat4 model, int i, int j);
glm::mat4 blackStoneGoBanToRealCoordinates(glm::mat4 model, int i, int j);
//My functions
void get_resolution(int &w, int &h);
void loadCubeMap(GLuint &texture);


using namespace std;
using namespace glm;

int main(int argc, char * argv[]) {

	// Load GLFW and Create a Window
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_SAMPLES, 4);
	int w, h;
	get_resolution(w, h);
	const unsigned int SCR_WIDTH = w;
	const unsigned int SCR_HEIGHT = h;
	GLFWwindow * window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "INFO-H-502 - Team Go !", nullptr, nullptr);

	// Check for Valid Context
	if (window == nullptr) {
		fprintf(stderr, "Failed to Create OpenGL Context");
		return EXIT_FAILURE;
	}

	// Create Context and Load OpenGL Functions
	glfwMakeContextCurrent(window);
	gladLoadGL();
	fprintf(stderr, "OpenGL %s\n", glGetString(GL_VERSION));
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_MULTISAMPLE);
	glEnable(GL_COLOR_MATERIAL);
	
	std::thread t([]() {
		GoGame::GoGame::getInstance().mainloop();
	});


	// Change Viewport
	int width, height;
	glfwGetFramebufferSize(window, &width, &height);
	glViewport(0, 0, width, height);
	// skybox shader
	Shader skyBoxShader{ "shaders/skybox.vert","shaders/skybox.frag" };
	skyBoxShader.compile();
	// tatami shader
	Shader tatamiShader{ "shaders/tatami.vert","shaders/tatami.frag" };
	tatamiShader.compile();
	// Light shader
	Shader lightShader{ "shaders/light.vert", "shaders/light.frag" };
	lightShader.compile();
	// goIshiShader
	Shader goIshiShader{ "shaders/goishishader.vert", "shaders/goishishader.frag" };
	goIshiShader.compile();
	// NM shader
	Shader normalsAndLightsShader { "shaders/light_and_normalmap.vert", 
								    "shaders/light_and_normalmap.frag" };
	normalsAndLightsShader.compile();

	Shader basicShader = { "shaders/default.vert", "shaders/default.frag" };
	basicShader.compile();

	Shader rayShader{ "shaders/ray.vert", "shaders/ray.frag" };
	rayShader.compile();

	// set shader properties
	tatamiShader.setLightingProperties(lightAmbienteness, lightDiffuseness, lightSpeculareness);
	tatamiShader.setMaterialProperties(materialSpeculareness, materialShininess);
	// set shader properties (for models using light shaders)
	lightShader.setLightingProperties(lightAmbienteness, lightDiffuseness, lightSpeculareness);
	lightShader.setMaterialProperties(materialSpeculareness, materialShininess);
	// set shader properties (for models using go ishi shaders)
	goIshiShader.setLightingProperties(lightAmbienteness, lightDiffuseness, lightSpeculareness);
	goIshiShader.setMaterialProperties(materialSpeculareness, materialShininess);

	normalsAndLightsShader.setLightingProperties(lightAmbienteness, lightDiffuseness, lightSpeculareness);
	normalsAndLightsShader.setMaterialProperties(materialSpeculareness, materialShininess);


	// ==== Light =============================================================
	// first, configure the light's VAO (and VBO)
	unsigned int lightVAO, lightVBO;

	glGenBuffers(1, &lightVBO);
	glBindBuffer(GL_ARRAY_BUFFER, lightVBO);
	// On utilise la skybox mais c'est juste un cube
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyBoxVertices), skyBoxVertices, GL_STATIC_DRAW);

	glGenVertexArrays(1, &lightVAO);
	glBindVertexArray(lightVAO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

	glBindBuffer(GL_ARRAY_BUFFER, lightVBO);
	// note that we update the lamp's position 
	// attribute's stride to reflect the updated buffer data
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	
	// ========================================================================

	// ..:: Initialization code (done once)
	GLuint skyBoxVAO, skyBoxVBO;
	glGenVertexArrays(1, &skyBoxVAO);
	// 1. Bind Vertex Array Object
	glBindVertexArray(skyBoxVAO);
	// 2. Copy our vertices array in a buffer for OpenGL to use
	glGenBuffers(1, &skyBoxVBO);
	glBindBuffer(GL_ARRAY_BUFFER, skyBoxVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyBoxVertices), skyBoxVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	// 3. Then set our vertex attributes pointers
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	
	//4. Unbind the VAO
	glBindVertexArray(0);

	std::vector <ALfloat> listenerOri = { 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f };
	std::vector <ALfloat> listenerPos = { 0, 0, 1.0f };
	audioMaster.setUpListener(listenerOri, listenerPos);
	audioMaster.configSources(listenerPos);

	GLuint skyboxTexture;
	loadCubeMap(skyboxTexture);



	Model goBan("goboard_obj/board.obj", "textures/goban_surface2.jpg", 
				"textures/goban_surface2_NRM.jpg", &normalsAndLightsShader);

	Model house("goboard_obj/house.obj", "textures/house2.png",
				"textures/house2_NRM.jpg", &normalsAndLightsShader);

	Model chair("goboard_obj/chair.obj", "textures/chair.png",
				"textures/chair_NRM.jpg", &normalsAndLightsShader);

	Model swords("goboard_obj/swords.obj", "textures/swords.png", 
				 "textures/swords_NRM.jpg", &normalsAndLightsShader);	

	Model tatami("goboard_obj/tatami.obj", "textures/tatami-final.png", 
				 "textures/tatami-final_NRM.jpg", &tatamiShader);

	Model shoji_closed("goboard_obj/shoji_closed.obj", "textures/shoji_closed2.png",
					   "textures/shoji_closed2_NRM.jpg", &normalsAndLightsShader);

	Model shoji_opened("goboard_obj/shoji_opened.obj", "textures/shoji_opened2.png",
					   "textures/shoji_opened2_NRM.jpg", &normalsAndLightsShader);

	
	Model cushion1("goboard_obj/cushion_1.obj", "textures/cushion_texture.png",
				   "textures/cushion_texture_NRM.jpg", &normalsAndLightsShader);
	Model cushion2("goboard_obj/cushion_2.obj", "textures/cushion_texture.png",
				   "textures/cushion_texture_NRM.jpg", &normalsAndLightsShader);

	Model bowls("goboard_obj/bowls.obj", "textures/bowls_lids.png");
	Model fan("goboard_obj/fan.obj", "textures/fan.png");
	Model blades("goboard_obj/blade.obj", "textures/blade.png");
	Model painting3("goboard_obj/painting_3.obj", "textures/painting_3.jpg");
	Model lamp("goboard_obj/lamp.obj", "textures/lamp_texture.jpg");

	// Les pierres
	Model whiteStone("goboard_obj/white_stone.obj", "textures/shell.jpg");
	Model blackStone("goboard_obj/black_stone.obj", "textures/slate.jpg");
	




	glm::mat4 projection = glm::perspective(camera.Zoom, (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.001f, 1000.0f);

	// Create a RayPicker 
	RayPicker rayCast(&cameraHaut, projection, (float)SCR_WIDTH, (float)SCR_HEIGHT);

	// Je n'initialise pas un nouvel objet car Events est un singleton
	// J'y ai �t� forc� car les callback n'acceptent que des fonctions
	// statiques. Donc, j'ai fais un workaround gr�ce au pattern singleton.
	Events &events = Events::initializeInstance(&camera, window, &rayCast, (float)SCR_WIDTH, (float)SCR_HEIGHT);

	
	float rayVertices[] = {
		// (x, y, z)		        // colors
		0.0f, -1.0f, 0.0f,         1.0f, 0.0f, 0.0f,  // v0
		1.68178f, 0.3f, 1.509f,    0.0f, 1.0f, 0.0f,  // v1
	};

	unsigned int VBO, VAO;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	// bind the Vertex Array Object first,
	// then bind and set vertex buffer(s), 
	// and then configure vertex attributes(s).
	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rayVertices), rayVertices, GL_STATIC_DRAW);

	// position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	// color attribute
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	skyBoxShader.use();
	skyBoxShader.setInteger("skybox", 0);

	normalsAndLightsShader.use();
	normalsAndLightsShader.setInteger("diffuseMap", 0);
	normalsAndLightsShader.setInteger("normalMap",  1);
	// Rendering Loop*/
	audioMaster.playBackgroundMusic();

	while (glfwWindowShouldClose(window) == false) {
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		events.showFPS();
		events.applyMovements();

		glm::mat4 model = glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0));
		projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.001f, 1000.0f);
		glm::mat4 view = getCurrentMatrixView();

		// Draw line ==========================================================
		// modify line according to ray casting
		glm::vec3 r = normalize(rayCast.currentRay);
		r *= 1000;
		rayVertices[0] = r.x;
		rayVertices[1] = r.y;
		rayVertices[2] = r.z;
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(rayVertices), rayVertices, GL_STATIC_DRAW);
		// render container
		glDepthFunc(GL_LEQUAL);
		rayShader.use();
		glBindVertexArray(VAO);
		glDrawArrays(GL_LINES, 0, 3);
		// create transformations
		rayShader.setMatrix4("projection", projection);
		rayShader.setMatrix4("model", model);
		rayShader.setMatrix4("view", view);
		glBindVertexArray(VAO);
		glBindVertexArray(0);
		glDepthFunc(GL_LESS);

		// Render light -------------------------------------------------------
		basicShader.use();
		// create transformations
		model = translate(mat4(1.0f), lightPos);
		model = scale(model, vec3(0.05f));

		basicShader.setMatrix4("projection", projection);
		basicShader.setMatrix4("model", model);
		basicShader.setMatrix4("view", view);

		glBindVertexArray(lightVAO);
		glDrawArrays(GL_TRIANGLES, 0, 36);
		// ====================================================================
		model = translate(mat4(1.0f), vec3(0, 0, 0));


		normalsAndLightsShader.setMaterialProperties(vec3(0.1f, 0.1f, 0.1f), 16.0f);
		goBan.DrawWithNormals(model, view, projection, camera, lightPos);
		
		normalsAndLightsShader.setMaterialProperties(vec3(0.8f, 0.8f, 0.8f), 16.0f);
		swords.DrawWithNormals(model, view, projection, camera, lightPos);

		normalsAndLightsShader.setMaterialProperties(materialSpeculareness, materialShininess);
		house.DrawWithNormals(model, view, projection, camera, lightPos);
		chair.DrawWithNormals(model, view, projection, camera, lightPos);
		tatami.DrawWithNormals(model, view, projection, camera, lightPos);

		normalsAndLightsShader.setLightingProperties(vec3(0.4f, 0.4f, 0.4f), lightDiffuseness, lightSpeculareness);
		normalsAndLightsShader.setMaterialProperties(vec3(0.0f, 0.0f, 0.0f), 16.0f);
		shoji_closed.DrawWithNormals(model, view, projection, camera, lightPos);
		shoji_opened.DrawWithNormals(model, view, projection, camera, lightPos);
		normalsAndLightsShader.setLightingProperties(lightAmbienteness, lightDiffuseness, lightSpeculareness);
		normalsAndLightsShader.setMaterialProperties(materialSpeculareness, materialShininess);

		
		cushion1.Draw(lightShader, model, view, projection, camera, lightPos);
		cushion2.Draw(lightShader, model, view, projection, camera, lightPos);
		lamp.Draw(lightShader, model, view, projection, camera, lightPos);
		lightShader.setLightingProperties(vec3(1.0f, 1.0f, 1.0f), lightDiffuseness, lightSpeculareness);
		painting3.Draw(lightShader, model, view, projection, camera, lightPos);
		lightShader.setLightingProperties(lightAmbienteness, lightDiffuseness, lightSpeculareness);

		bowls.Draw(lightShader, model, view, projection, camera, lightPos);

		

		// Draw each stones
		for (int i = 0; i < 19; i++) {
			for (int j = 0; j < 19; j++) {
				if (GoGame::getInstance().goBan[i][j] == 'O') {
					glm::mat4 modelInGoBan = whiteStoneGoBanToRealCoordinates(model, i, j); 
					whiteStone.Draw(goIshiShader, modelInGoBan, view, projection, camera, lightPos);
				}
				else if (GoGame::getInstance().goBan[i][j] == 'X') {
					glm::mat4 modelInGoBan = blackStoneGoBanToRealCoordinates(model, i, j);
					blackStone.Draw(goIshiShader, modelInGoBan, view, projection, camera, lightPos);
				}
			}
		}
		fan.Draw(lightShader, model, view, projection, camera, lightPos);

		model = glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0));
		model = glm::rotate(model, (float) glfwGetTime() * 20.0f, glm::vec3(3.165f, 0.26235f, 1.94358f));
		blades.Draw(lightShader, model, view, projection, camera, lightPos);

		// Draw skybox ========================================================
		glDepthFunc(GL_LEQUAL);

		skyBoxShader.use();
		view = glm::mat4(glm::mat3(camera.GetViewMatrix()));
		projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.001f, 1000.0f);
		skyBoxShader.setMatrix4("projection", projection);
		skyBoxShader.setMatrix4("view", view);
		glBindVertexArray(skyBoxVAO);
		glBindTexture(GL_TEXTURE_CUBE_MAP, skyboxTexture);
		glDrawArrays(GL_TRIANGLES, 0, 36); // draw your skybox
		glBindVertexArray(0);
		glDepthFunc(GL_LESS);

		// Flip Buffers and Draw
		glfwSwapBuffers(window);
		events.pollEvents();
	}
	glfwTerminate();
	// close all threads.
	GoGame::getInstance().terminate();
	t.join();
	return EXIT_SUCCESS;
}

glm::mat4 getCurrentMatrixView()
{
	Events &e = Events::getInstance();
	switch (e.indexCurrentCamera)
	{
		case 0: return camera.GetViewMatrix();
		case 1: return cameraHaut.GetViewMatrix();
		case 2: return cameraSwagg.GetViewMatrix();
		default: return camera.GetViewMatrix();
	}
}


void loadCubeMap(GLuint &texture) {
	// std::vector<std::string> textures = { "posx.jpg", "negx.jpg", "posy.jpg", "negy.jpg", "posz.jpg", "negz.jpg" };
	std::vector<std::string> textures = { "px.jpg", "nx.jpg", "py.jpg", "ny.jpg", "pz.jpg", "nz.jpg" };
	// Must be 6 images
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	int width, height,n;
	unsigned char* image;
	std::string folder = "textures/cubemap/";
	for (GLuint i = 0; i < textures.size(); i++) {
		image = stbi_load((folder + textures[i]).c_str(), &width, &height,&n,0);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
		stbi_image_free(image);
	}
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}


glm::mat4 whiteStoneGoBanToRealCoordinates(glm::mat4 model, int i, int j)
{
	float offsetX = 0.023366872f;
	float offsetZ = 0.021618733f;
	float dx = 3 * offsetX;
	float dz = 3 * offsetZ;
	return glm::translate(model, glm::vec3{ -i*offsetX + dx, 0.0f, -j*offsetZ + dz });
}

glm::mat4 blackStoneGoBanToRealCoordinates(glm::mat4 model, int i, int j)
{
	float offsetX = 0.023366872f;
	float offsetZ = 0.021618733f;
	float dx = 15 * offsetX;
	float dz = 15 * offsetZ;
	return glm::translate(model, glm::vec3{ -i*offsetX + dx, 0.0f, -j*offsetZ + dz });
}

void get_resolution(int &w, int &h) {
	const GLFWvidmode * mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	w = mode->width;
	h = mode->height;
}