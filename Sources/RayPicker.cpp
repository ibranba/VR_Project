#include "RayPicker.hpp"


RayPicker::RayPicker(Camera * c, glm::mat4 & p, float screenWidth, float screenHeight)
{
	this->camera = c;
	this->projection = p;
	this->screenWidth = screenWidth;
	this->screenHeight = screenHeight;
}

glm::vec3 RayPicker::updateRay(double xpos, double ypos)
{
	// 1. Nous d�marrons avec les coordonn�es 
	// (xpos, ypos) de la souris. 
	// Nous les recevons en param�tre. 
	// Elles sont en viewport coordinates.
	// x -> [0:SCR_WIDTH] et y -> [0:SCR_HEIGHT]

	// 2. Nous transformons ces coordonn�es 
	// 2D en normalized device coordinates 3D
	// avec z = 1 comme troisi�me composante.
	glm::vec3 rayNormalizedDeviceCoordinate { 
		(2.0f * xpos) / this->screenWidth - 1.0f,  // x -> [-1:1]
		1.0f - (2.0f * ypos) / this->screenHeight, // y -> [-1:1]
		1.0f									   // z -> [-1:1]
	};

	// 3. Faire pointer la coordonn�e Z de 
	// notre point droit devant. Avec Opengl,
	// il suffit de mettre z � -1. Nous ajoutons �galement 
	// une quatri�me composante w �gale � 1. 
	// Nous avons ainsi un vecteur 4D homog�ne normalis�.
	glm::vec4 ray_clip {
		rayNormalizedDeviceCoordinate.x,		 // x -> [-1:1]
		rayNormalizedDeviceCoordinate.y,		 // y -> [-1:1]
		-1.0,									 // z -> [-1:1]
		1.0										 // w -> [-1:1]
	};

	// 4. Pour aller du eye space vers le clip space, 
	// nous multiplions par la matrice projection. 
	// Nous voulons faire le chemin inverse, 
	// donc nous multiplions par l'inverse de la
	// matrice projection.
	glm::mat4 inv = inverse(this->projection);
	glm::vec4 ray_eye = inv * ray_clip;
	ray_eye = glm::vec4 { 
		ray_eye.x, 
		ray_eye.y, 
		-1.0f,
		0.0f 
	};

	// 5. Go en world space ! On multiplie le vecteur
	// de l'�tape pr�c�dente par l'inverse 
	// de la matrice view. 
	glm::mat4 view = camera->GetViewMatrix();
	glm::vec4 res = inverse(view) * ray_eye;
	glm::vec3 ray_world { 
		res.x, 
		res.y, 
		res.z 
	};
	ray_world = normalize(ray_world);
	this->currentRay = ray_world;
	return ray_world;
}

