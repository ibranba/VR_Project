#include "Events.hpp"

using namespace std;

// encapsulation my ass

Events * Events::instance = new Events(NULL, NULL, NULL, -1, -1);

Events &Events::initializeInstance(Camera * c, GLFWwindow * w, RayPicker *rayCast, GLuint width, GLuint height)
{
	Events::instance = new Events(c, w, rayCast, width, height);
	return *instance;
}

Events::Events(Camera * c, GLFWwindow *w, RayPicker *r, GLuint screenWidth, GLuint screenHeight)
{
	if (c != NULL && w != NULL && r != NULL) 
	{
		camera = c;
		window = w;
		deltaTime = 0.0f;
		firstMouse = true;
		lastX = 400;
		lastY = 300;
		this->screenHeight = screenHeight;
		this->screenWidth = screenWidth;
		this->rayCast = r;
		this->indexCurrentCamera = 0;
		// Set the required callback functions
		this->setKeyListener();
		this->setCursorPositionListener();
		this->setMouseButtonListener();
		this->setScrollEventListener();
	}
}

glm::vec3 Events::computeIntersectionWithGoBan(glm::vec3 p0, glm::vec3 p1)
{
	// point in the plan
	glm::vec3 p2(0.0f, -0.31f, 0.0f);

	p2 = normalize(p2);
	p2 *= 1000;
	// normal of the plan -- pointe vers l'axe y
	glm::vec3 n{ 0.0f, 1.0f, 0.0f };
	// The goban's plan equation is
	// n.x(x - p2.x) + n.y(p2.y - y) + n.z(p2.z - z) = 0
	// the intersection well be in the form 
	// I = <(p2.x - p1.x)t, (p2.y - p1.y)t, (p2.z - p1.z)t>
	float t =
		(n.x * p2.x + n.y * p2.y + n.z * p2.z) /
		//  ---------------------------------------------------------------
		(n.x*p1.x - n.x*p0.x + n.y*p1.y - n.y*p0.y + n.z*p1.z - n.z*p0.z);

	glm::vec3 intersection = {
		p0.x + (p1.x - p0.x)*t,
		p0.y + (p1.y - p0.y)*t,
		p0.z + (p1.z - p0.z)*t
	};
	return intersection;
}

Events::~Events()
{
	// Should I do this ?

	// free(this->camera);
	// this->camera = NULL;
	// free(this->window);
	// this->window = NULL;

}

Events & Events::getInstance()
{
	return *instance;
}

void Events::applyMovements()
{

	// movements
	if (this->indexCurrentCamera == 0)
	{
		if (keys[GLFW_KEY_W]) camera->ProcessKeyboard(FORWARD, this->deltaTime);
		if (keys[GLFW_KEY_S]) camera->ProcessKeyboard(BACKWARD, this->deltaTime);
		if (keys[GLFW_KEY_A]) camera->ProcessKeyboard(LEFT, this->deltaTime);
		if (keys[GLFW_KEY_D]) camera->ProcessKeyboard(RIGHT, this->deltaTime);
	}

	if (this->indexCurrentCamera == 1)
	{
		// play go
		if (keys[GLFW_KEY_SPACE])
		{
			int widthSquare = 38728;
			int heightSquare = 36155;
			glm::vec3 r = normalize(rayCast->currentRay);
			glm::vec3 n = normalize(glm::vec3{ 1.68178f, 0.14f, 1.509f });
			r *= 1000;
			glm::vec3 i = computeIntersectionWithGoBan(n, r);
			i *= 1000;
			int l = floor(abs(((i.x - widthSquare) / widthSquare) - 9));
			int c = floor(abs(((i.z) / heightSquare) - 9));
			keys[GLFW_MOUSE_BUTTON_LEFT] = false;
			std::string coordinate = std::to_string(l) + ";" + std::to_string(c);
			GoGame::getInstance().send(coordinate);
			keys[GLFW_KEY_SPACE] = false;
		}
	}

	if (keys[GLFW_KEY_TAB])
	{
		int nbCameras = 3;
		this->indexCurrentCamera = (this->indexCurrentCamera + 1) % nbCameras;
		keys[GLFW_KEY_TAB] = false;
	}


}


void Events::showFPS()
{
	static double lastTime = glfwGetTime();
	static int nbFrames = 0;

	// Measure speed
	double currentTime = glfwGetTime();
	this->deltaTime = currentTime - lastTime;
	nbFrames++;
	if (currentTime - lastTime >= 1.0) { // If last prinf() was more than 1 sec ago
											// printf and reset timer
		std::cout << 1000.0 / double(nbFrames) << " ms/frame -> " << nbFrames << " frames/sec" << std::endl;
		nbFrames = 0;
		lastTime += 1.0;
	}
}


void Events::staticKeyHandler(GLFWwindow * window, int key, int scancode, int action, int mods)
{
	Events::getInstance().defaultKeyHandler(key, scancode, action, mods);
}

void Events::staticMouseButtonHandler(GLFWwindow * window, int button, int action, int mods)
{
	Events::getInstance().defaultMouseButtonHandler(button, action, mods);
}

void Events::staticCursorPositionHandler(GLFWwindow * window, double xpos, double ypos)
{
	Events::getInstance().defaultCursorPositionHandler(xpos, ypos);
}

void Events::staticScrollHandler(GLFWwindow * window, double xoffset, double yoffset)
{
	Events::getInstance().defaultScrollHandler(xoffset, yoffset);
}

void Events::defaultKeyHandler(int key, int scancode, int action, int mods)
{
	cout << "click !" << endl;

	if (action == GLFW_PRESS)
		keys[key] = true;
	else if (action == GLFW_RELEASE)
		keys[key] = false;

	// Touche espace
	if (action == GLFW_KEY_SPACE)
		keys[key] = true;

	// Touche Tab
	if (action == GLFW_KEY_TAB)
		keys[key] = true;
	
	if (keys[GLFW_KEY_ESCAPE])
		glfwSetWindowShouldClose(window, GL_TRUE);

	// V-SYNC
	if (keys[GLFW_KEY_U]) {
		static bool vsync = true;
		if (vsync) {
			glfwSwapInterval(1);
		}
		else {
			glfwSwapInterval(0);
		}
		vsync = !vsync;
	}

	if ((keys[GLFW_KEY_0] || keys[GLFW_KEY_KP_0])) {
		std::cout << "You have pressed 0" << std::endl;
	}


}

void Events::defaultMouseButtonHandler(int button, int action, int mods) 
{
	if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
		keys[GLFW_MOUSE_BUTTON_RIGHT] = true;
	else
		keys[GLFW_MOUSE_BUTTON_RIGHT] = false;

	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
		keys[GLFW_MOUSE_BUTTON_LEFT] = true;
	else
		keys[GLFW_MOUSE_BUTTON_LEFT] = false;

	if (button == GLFW_MOUSE_BUTTON_MIDDLE && action == GLFW_PRESS)
		keys[GLFW_MOUSE_BUTTON_MIDDLE] = true;
	else
		keys[GLFW_MOUSE_BUTTON_MIDDLE] = false;
}

void Events::defaultCursorPositionHandler(double xpos, double ypos)
{
	if (this->indexCurrentCamera == 0)
	{
		if (keys[GLFW_MOUSE_BUTTON_LEFT]) {
			if (firstMouse) {
				lastX = xpos;
				lastY = ypos;
				firstMouse = false;
			}
			GLfloat xoffset = xpos - lastX;
			GLfloat yoffset = lastY - ypos;  // Reversed since y-coordinates go from bottom to left        
			lastX = xpos; 
			lastY = ypos;
			camera->ProcessMouseMovement(xoffset, yoffset);
		}
	}
	if (this->indexCurrentCamera == 1) {
		rayCast->updateRay(xpos, ypos);
	}
}

void Events::defaultScrollHandler(double xoffset, double yoffset)
{
	camera->ProcessMouseScroll(yoffset);
}

void Events::setKeyListener(GLFWkeyfun handler)
{
	GLFWkeyfun f = handler != NULL ? handler : staticKeyHandler;
	glfwSetKeyCallback(this->window, f);
}

void Events::setCursorPositionListener(GLFWcursorposfun handler)
{
	GLFWcursorposfun f = handler != NULL ? handler : staticCursorPositionHandler;
	glfwSetCursorPosCallback(this->window, f);
}

void Events::setMouseButtonListener(GLFWmousebuttonfun handler)
{
	GLFWmousebuttonfun f = handler != NULL ? handler : staticMouseButtonHandler;
	glfwSetMouseButtonCallback(this->window, f);
}

void Events::setScrollEventListener(GLFWscrollfun handler)
{
	GLFWscrollfun f = handler != NULL ? handler : staticScrollHandler;
	glfwSetScrollCallback(this->window, f);
}

void Events::pollEvents()
{
	glfwPollEvents();
}


